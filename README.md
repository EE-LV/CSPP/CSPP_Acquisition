CS++ Acquisition README
=======================
This package contains all CS++ actors for data acquisition. Note that additional device drivers might be needed as well.

| Acquistion class name | type | device driver | additional Ini-file entries |
| --------------------- | ---- | ------------- | --------------------------- |
| CSPP_TDC8HP           | driver | [TDC8HP](https://git.gsi.de/EE-LV/Drivers/TDC8HP) | see below |

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.

TDC8HP
------
This actor controls the TDC8HP used by the RoentDek detecor for PI-ICR measurement in the trap community. First, an additional TDC driver has to downloaded from
[RoentDek](http://www.roentdek.com/software/drivers/TDC8HP_3.7.0-2.zip). This driver runs under Win10 64 bit. The [LabVIEW driver](https://git.gsi.de/EE-LV/Drivers/TDC8HP) uses the RoentDek dlls to communicate with
the TDC card.

The hardware channel assignments can be found in the documentation of the LabVIEW driver. The idea of this detector system is that each MCP event will trigger 4 additional
events on two delay lines used to reconstruct the x-y position of the ion impact on the detector. The result is a 2D picture like this:

![A 2D picture of detected MCP signals acquired with a RoentDek detector](Docs/example_tdc.png)

The software configuration of a TDC8HP actor is rather simple. After installing the two necessary drivers the following entries in the Ini-file have to be adjusted:

```
[myTDC8HP]
...
CSPP_DeviceActor:CSPP_DeviceActor.ResourceName=""
CSPP_TDC8HP:CSPP_TDC8HP.GroupRangeStart_ns=0
CSPP_TDC8HP:CSPP_TDC8HP.GroupRangeStop_ns=70
CSPP_TDC8HP:CSPP_TDC8HP.SimulationMode=False
CSPP_TDC8HP:CSPP_TDC8HP.PublishInterval_s=1
```

The ResourceName attribute can stay empty for this device (but the key cannot be deleted!). The GroupRange (here 0-70 ns) has to be used to adjust the detector to the expected
ion signal. It defines a time window around the MCP signal around position signals are accepted. For a detailed description about the setting up of the detector see
the TDC8HP manual on the RoentDek [webpage](https://www.roentdek.com/manuals/). The simulation mode boolean switches on and off a simulation mode within the RoentDek dll and the
PublishInterval defines the interval the actor asks the RoentDek dll for new events. 

The URLs of the TDC8HP actor publish some status information. Especially an event number as well as some booleans indicating whether the detecor is ready for data aqcuistion
can be see:

```
[myTDC8HP.URLs]
Error="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_Error"
SimulationMode="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_SimulationMode"
GroupStart="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_GroupStart"
GroupStop="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_GroupStop"
ActEvent="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_ActEvent"
TDCStarted="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_TDCStarted"
TDCPaused="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_TDCPaused"
TDCBlocked="ni.var.psp://localhost/CSPP_Acquisition_SV/myTDC8HP_TDCBlocked"
```

The TDC8HP actor class has also a default GUI *TDC8HPGui* which can be used for commisioning. 

Related information
---------------------------------
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.